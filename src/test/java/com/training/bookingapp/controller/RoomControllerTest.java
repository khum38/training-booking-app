package com.training.bookingapp.controller;

import java.sql.Time;
import java.util.List;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.training.bookingapp.model.CreateRoomRequest;
import com.training.bookingapp.model.RegisterRequest;
import com.training.bookingapp.repository.RoomRepository;
import com.training.bookingapp.repository.UserRepository;
import com.training.bookingapp.dbmodel.Room;
import com.training.bookingapp.dbmodel.User;

import io.restassured.RestAssured;
import net.minidev.json.JSONObject;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomControllerTest {

    @Autowired
    protected RoomRepository roomRepository;

    @Autowired
    protected UserRepository userRepository;

    @LocalServerPort
    protected int port;

    protected String emailTest = "test@test.com";
    protected String roomName = "Room-Test";
    protected String jwtToken;

    @BeforeEach
    public void setUp() throws Exception {
        RestAssured.port = port;

        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        var response = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        this.jwtToken = response.getString("token");
    }

    @AfterEach  
    public void tearDown() throws Exception {
        List<Room> rooms = this.roomRepository.findByName(this.roomName).orElse(null);
        if (rooms != null) {
            this.roomRepository.deleteAll(rooms);
        }

        User user = this.userRepository.findByEmail(this.emailTest).orElse(null);
        if (user != null) {
            this.userRepository.delete(user);
        }
    }

    @Test
    public void testCreateRoom_success(){
        CreateRoomRequest createRoomRequest = CreateRoomRequest.builder()
            .name(this.roomName)
            .description("Room for test")
            .capacity(10)
            .openTime(Time.valueOf("08:00:00"))
            .closeTime(Time.valueOf("18:00:00"))
            .build();

        var response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(createRoomRequest)
            .post("/api/rooms")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getString("name")).isEqualTo(this.roomName);
        Assertions.assertThat(response.getString("description")).isEqualTo("Room for test");
        Assertions.assertThat(response.getInt("capacity")).isEqualTo(10);
        Assertions.assertThat(response.getInt("id")).isNotNull();
    }

    @Test
    public void testCreateRoom_error(){
        JSONObject createRoomRequest = new JSONObject();
        createRoomRequest.put("name", this.roomName);
        createRoomRequest.put("description", "Room for test");
        createRoomRequest.put("capacity", "xxx");
        createRoomRequest.put("openTime", Time.valueOf("08:00:00"));
        createRoomRequest.put("closeTime", Time.valueOf("18:00:00"));

        var response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(createRoomRequest)
            .post("/api/rooms")
            .then()
            .statusCode(500)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getString("errorCode")).isEqualTo("00");
    }
}
