package com.training.bookingapp.controller;

import io.restassured.RestAssured;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import com.training.bookingapp.dbmodel.User;
import com.training.bookingapp.model.AuthenticationRequest;
import com.training.bookingapp.model.RegisterRequest;
import com.training.bookingapp.repository.UserRepository;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {
    
    @Autowired
    private UserRepository userRepository;

    @LocalServerPort
    private int port;

    private String emailTest = "test@test.com";

    @BeforeEach
    public void setUp() throws Exception {
        RestAssured.port = port;
    }

    @AfterEach  
    public void tearDown() throws Exception {
        User user = this.userRepository.findByEmail(this.emailTest).orElse(null);
        if (user != null) {
            this.userRepository.delete(user);
        }
    }

    @Test
    public void testRegisterUser_success() {
        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        var response = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getString("token")).isNotBlank();
    }

    @Test
    public void testRegisterDuplicateUser_error(){
        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        var response = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getString("token")).isNotBlank();

        var response2 = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(400)
            .extract()
            .jsonPath();

        Assertions.assertThat(response2.getString("message")).isEqualTo("User already exists");
        Assertions.assertThat(response2.getString("errorCode")).isEqualTo("A001");
        Assertions.assertThat(response2.getString("details")).isEqualTo("User " + this.emailTest + " already exists");
    }

    @Test
    public void testUserAuthenticate_success(){
        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
            .email(registerRequest.getEmail())
            .password(registerRequest.getPassword())
            .build();

        var registerResponse = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(registerResponse.getString("token")).isNotBlank();

        var authenResponse = RestAssured.given()
            .contentType("application/json")
            .body(authenticationRequest)
            .post("/api/users/authenticate")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(authenResponse.getString("token")).isNotBlank();
    }

    @Test
    public void testUserAuthenticateInvalidCredential_error(){
        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
            .email(registerRequest.getEmail())
            .password("wrong password")
            .build();

        var registerResponse = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(registerResponse.getString("token")).isNotBlank();

        var authenResponse = RestAssured.given()
            .contentType("application/json")
            .body(authenticationRequest)
            .post("/api/users/authenticate")
            .then()
            .statusCode(400)
            .extract()
            .jsonPath();

        Assertions.assertThat(authenResponse.getString("message")).isEqualTo("Invalid credentials");
        Assertions.assertThat(authenResponse.getString("errorCode")).isEqualTo("A002");
        Assertions.assertThat(authenResponse.getString("details")).isEqualTo("Invalid credentials");
    }

    @Test
    public void testUserAuthenticateUserNotfound_error(){
        RegisterRequest registerRequest = RegisterRequest.builder()
            .email(this.emailTest)
            .password("test")
            .firstName("test")
            .lastName("test")
            .build();

        AuthenticationRequest authenticationRequest = AuthenticationRequest.builder()
            .email("test-notfound@test.com")
            .password("wrong password")
            .build();

        var registerResponse = RestAssured.given()
            .contentType("application/json")
            .body(registerRequest)
            .post("/api/users/register")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(registerResponse.getString("token")).isNotBlank();

        var authenResponse = RestAssured.given()
            .contentType("application/json")
            .body(authenticationRequest)
            .post("/api/users/authenticate")
            .then()
            .statusCode(404)
            .extract()
            .jsonPath();
        Assertions.assertThat(authenResponse.getString("message")).isEqualTo("User not found");
        Assertions.assertThat(authenResponse.getString("errorCode")).isEqualTo("A003");
        Assertions.assertThat(authenResponse.getString("details")).isEqualTo("User not found");
    }
}
