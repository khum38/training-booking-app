package com.training.bookingapp.controller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;

public class BookingControllerTest extends RoomControllerActivityTest {
    
    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
    }

    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testGetBooking_success(){
        var responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/bookings/" + this.bookingId1)
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(responseJson.getInt("id")).isNotNull();
        Assertions.assertThat(responseJson.getString("title")).isEqualTo(this.bookingTitle1);
        Assertions.assertThat(responseJson.getString("organizer.email")).isEqualTo(this.emailTest);
        Assertions.assertThat(responseJson.getInt("room.id")).isEqualTo(this.roomId1);
    }

    @Test
    public void testGetBookings_error(){
        var responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/bookings/0")
            .then()
            .statusCode(404)
            .extract()
            .jsonPath();
    
        Assertions.assertThat(responseJson.getString("message")).isEqualTo("Booking not found");
        Assertions.assertThat(responseJson.getString("errorCode")).isEqualTo("B001");
    }

    @Test
    public void testGetBookingsByOrganizer_success(){
        var responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/bookings/")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(responseJson.getInt("[0].id")).isNotNull();
        Assertions.assertThat(responseJson.getString("[0].title")).isEqualTo(this.bookingTitle1);
        Assertions.assertThat(responseJson.getString("[0].organizer.email")).isEqualTo(this.emailTest);
        Assertions.assertThat(responseJson.getInt("[0].room.id")).isEqualTo(this.roomId1);
    }

    @Test
    public void testDeleteBooking_success(){
        var response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .delete("/api/bookings/" + this.bookingId1)
            .then()
            .statusCode(200)
            .extract().asString();

        Assertions.assertThat(response).isEqualTo("Booking deleted");
    }

}
