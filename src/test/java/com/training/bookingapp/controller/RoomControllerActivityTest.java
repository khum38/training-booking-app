package com.training.bookingapp.controller;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import com.training.bookingapp.dbmodel.Room;
import com.training.bookingapp.model.BookingRequest;
import com.training.bookingapp.model.CreateRoomRequest;
import com.training.bookingapp.model.SearchAvilableRoomRequest;
import com.training.bookingapp.repository.BookingRepository;


public class RoomControllerActivityTest extends RoomControllerTest {

    @Autowired
    protected BookingRepository bookingRepository;

    protected Integer roomId1;
    protected Integer roomId2;
    protected Integer roomCount;

    protected String roomName2 = "Room-Test-2";

    protected String bookingTitle1 = "Test Booking 1";
    protected String bookingTitle2 = "Test Booking 2";
    protected Integer bookingId1;
    
    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();

        CreateRoomRequest createRoomRequest = CreateRoomRequest.builder()
            .name(this.roomName)
            .description("Room for test")
            .capacity(10)
            .openTime(Time.valueOf("08:00:00"))
            .closeTime(Time.valueOf("18:00:00"))
            .build();

        JsonPath response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(createRoomRequest)
            .post("/api/rooms")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        this.roomId1 = response.getInt("id");

        CreateRoomRequest createRoomRequest2 = CreateRoomRequest.builder()
            .name(this.roomName2)
            .description("Room for test 2")
            .capacity(5)
            .openTime(Time.valueOf("10:00:00"))
            .closeTime(Time.valueOf("20:00:00"))
            .build();

        var response2 = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(createRoomRequest2)
            .post("/api/rooms")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        this.roomId2 = response2.getInt("id");

        this.roomCount = this.roomRepository.findAll().size();

        BookingRequest bookingRequest = BookingRequest.builder()
            .title(this.bookingTitle1)
            .description("test test test")
            .date(Date.valueOf("2023-01-01"))
            .startTime(LocalDateTime.of(2023, 1, 1, 10, 0, 0))
            .endTime(LocalDateTime.of(2023, 1, 1, 11, 0, 0))
            .guestNumber(10)
            .build();


        JsonPath responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(bookingRequest)
            .post("/api/rooms/" + this.roomId1 + "/book")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        this.bookingId1 = responseJson.getInt("id");

    }

    @AfterEach
    public void tearDown() throws Exception {
        this.bookingRepository.findAllByRoomId(this.roomId1).orElse(null).forEach(booking -> {
            this.bookingRepository.deleteById(booking.getId());
        });

        this.bookingRepository.findAllByRoomId(this.roomId2).orElse(null).forEach(booking -> {
            this.bookingRepository.deleteById(booking.getId());
        });

        List<Room> rooms = this.roomRepository.findByName(this.roomName2).orElse(null);
        if (rooms != null) {
            this.roomRepository.deleteAll(rooms);
        }

        super.tearDown();
    }

    @Test
    public void testGetRoomInformation_success(){
        JsonPath response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/rooms/" + this.roomId2)
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getInt("id")).isEqualTo(this.roomId2);
        Assertions.assertThat(response.getString("name")).isEqualTo(this.roomName2);
        Assertions.assertThat(response.getString("description")).isEqualTo("Room for test 2");
        Assertions.assertThat(response.getInt("capacity")).isEqualTo(5);
        Assertions.assertThat(response.getString("openTime")).isEqualTo("10:00:00");
        Assertions.assertThat(response.getString("closeTime")).isEqualTo("20:00:00");
    }

    @Test
    public void testGetRoomInformation_error(){
        JsonPath response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/rooms/999999")
            .then()
            .statusCode(404)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getString("message")).isEqualTo("Room not found");
        Assertions.assertThat(response.getString("errorCode")).isEqualTo("R001");
    }

    @Test
    public void testGetAllRoomInformation_success(){
        JsonPath response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .get("/api/rooms")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(response.getList("id").size()).isEqualTo(this.roomCount);
    }

    @Test
    public void testBookingRoom_success(){
        BookingRequest bookingRequest = BookingRequest.builder()
            .title(this.bookingTitle1)
            .description("test test test")
            .date(Date.valueOf("2023-01-01"))
            .startTime(LocalDateTime.of(2023, 1, 1, 11, 0, 0))
            .endTime(LocalDateTime.of(2023, 1, 1, 13, 0, 0))
            .guestNumber(10)
            .build();


        JsonPath responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(bookingRequest)
            .post("/api/rooms/" + this.roomId1 + "/book")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        Assertions.assertThat(responseJson.getInt("id")).isNotNull();
        Assertions.assertThat(responseJson.getString("title")).isEqualTo(this.bookingTitle1);
        Assertions.assertThat(responseJson.getString("startDateTime")).isEqualTo("2023-01-01T11:00:00");
        Assertions.assertThat(responseJson.getString("endDateTime")).isEqualTo("2023-01-01T13:00:00");
        Assertions.assertThat(responseJson.getString("organizer.email")).isEqualTo(this.emailTest);
        Assertions.assertThat(responseJson.getInt("room.id")).isEqualTo(this.roomId1);
    }

    @Test
    public void testBookingRoom_error(){
        BookingRequest bookingRequest = BookingRequest.builder()
            .title(this.bookingTitle1)
            .description("test test test")
            .date(Date.valueOf("2023-01-01"))
            .startTime(LocalDateTime.of(2023, 1, 1, 10, 0, 0))
            .endTime(LocalDateTime.of(2023, 1, 1, 11, 0, 0))
            .guestNumber(10)
            .build();


        JsonPath responseJson = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(bookingRequest)
            .post("/api/rooms/" + this.roomId1 + "/book")
            .then()
            .statusCode(404)
            .extract()
            .jsonPath();

        Assertions.assertThat(responseJson.getString("message")).isEqualTo("Room not available");
        Assertions.assertThat(responseJson.getString("errorCode")).isEqualTo("R002");
    }

    @Test
    public void testSearchRoomAvliable_success(){
        SearchAvilableRoomRequest searchRoomRequest = SearchAvilableRoomRequest.builder()
            .guestNumber(5)
            .date(Date.valueOf("2023-01-01"))
            .startTime(LocalDateTime.of(2023, 1, 1, 10, 0, 0))
            .endTime(LocalDateTime.of(2023, 1, 1, 17, 0, 0))
            .build();

        JsonPath response = RestAssured.given()
            .contentType("application/json")
            .header("Authorization", "Bearer " + this.jwtToken)
            .body(searchRoomRequest)
            .post("/api/rooms/search")
            .then()
            .statusCode(200)
            .extract()
            .jsonPath();

        List<Integer> ids = response.getList("id");

        Assertions.assertThat(ids.contains(this.roomId1)).isEqualTo(false);
        Assertions.assertThat(ids.contains(this.roomId2)).isEqualTo(true);
    }
    
}
