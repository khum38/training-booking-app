package com.training.bookingapp.repository;

import com.training.bookingapp.dbmodel.Booking;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
    
    Optional<List<Booking>> findAllByRoomId(Integer roomId);
    Optional<List<Booking>> findAllByOrganizerId(Integer organizerId);
    void deleteById(Integer id);
    
}
