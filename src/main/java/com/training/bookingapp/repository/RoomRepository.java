package com.training.bookingapp.repository;


import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.List;
import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.training.bookingapp.dbmodel.Room;


@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
    // Optional<List<Room>> findAllMoreThanCapacity(Integer capacity);

    Optional<List<Room>> findByName(String name);

    @Query(value = """
        SELECT distinct r.id as r_id, r.*
        from room r 
        left join booking b on b.room_id = r.id
        where r.capacity >= :capacity and r.id not in (
            select b.room_id 
            from booking b 
            where (:startTime < end_date_time and :endTime >= end_date_time) or ( start_date_time >= :startTime and start_date_time < :endTime ) or ( end_date_time <= :endTime and start_date_time >= :startTime)
        )
    """, nativeQuery = true)
    Optional<List<Room>> findAllByCapacityAndAvilable(Integer capacity, LocalDateTime startTime, LocalDateTime endTime);

}
