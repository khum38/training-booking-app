package com.training.bookingapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.http.ResponseEntity;

import com.training.bookingapp.config.JwtService;
import com.training.bookingapp.dbmodel.Booking;
import com.training.bookingapp.dbmodel.User;
import com.training.bookingapp.model.BookingResponse;
import com.training.bookingapp.service.AuthService;
import com.training.bookingapp.service.BookingService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/api/bookings")
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;
    private final JwtService jwtService;
    private final AuthService authService;

    @GetMapping("/")
    public ResponseEntity<List<BookingResponse>> getBookingByOrganizerId(
        @RequestHeader(value = "Authorization") String accessToken
    ) {
        String email = jwtService.extractUserEmail(accessToken.substring(7));
        User user = authService.getUserByEmail(email);

        List<Booking> bookings =  this.bookingService.getBookingByOrganizerId(user.getId());
        List<BookingResponse> bookingResponses = new ArrayList<>();

        for (Booking booking : bookings) {
            bookingResponses.add(this.bookingService.convertBookingToBookingResponse(booking));
        }
        return ResponseEntity.ok(bookingResponses);
    }

    @GetMapping("/{bookingId}")
    public ResponseEntity<BookingResponse> getBookingById(
        @PathVariable("bookingId") Integer bookingId
    ) {
        Booking booking =  this.bookingService.getBookingById(bookingId);
        BookingResponse bookingResponse = this.bookingService.convertBookingToBookingResponse(booking);
        return ResponseEntity.ok(bookingResponse);
    }

    @DeleteMapping("/{bookingId}")
    public ResponseEntity<String> deleteBookingById(
        @PathVariable("bookingId") Integer bookingId
    ) {
        this.bookingService.deleteBookingById(bookingId);
        return ResponseEntity.ok("Booking deleted");
    }
}
