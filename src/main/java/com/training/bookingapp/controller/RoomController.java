package com.training.bookingapp.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

import java.util.List;

import com.training.bookingapp.config.JwtService;
import com.training.bookingapp.dbmodel.Booking;
import com.training.bookingapp.dbmodel.Room;
import com.training.bookingapp.dbmodel.User;

import com.training.bookingapp.model.BookingRequest;
import com.training.bookingapp.model.BookingResponse;
import com.training.bookingapp.model.CreateRoomRequest;
import com.training.bookingapp.model.RoomInformationResponse;
import com.training.bookingapp.model.SearchAvilableRoomRequest;

import com.training.bookingapp.service.AuthService;
import com.training.bookingapp.service.RoomService;
import com.training.bookingapp.service.BookingService;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping(value = "/api/rooms")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;
    private final BookingService bookingService;
    private final JwtService jwtService;
    private final AuthService authService;
    
    @GetMapping
    public List<RoomInformationResponse> getAllRooms() {
        List<Room> rooms = roomService.getAllRooms();
        List<RoomInformationResponse> roomInformationResponses = convertRoomToRoomInformationResponse(rooms);
        return roomInformationResponses;
    }

    private List<RoomInformationResponse> convertRoomToRoomInformationResponse(List<Room> rooms) {
        return rooms.stream().map(room -> {
            return roomService.convertRoomToRoomInformationResponse(room);
        }).toList();
    }

    @PostMapping
    public ResponseEntity<Room> createRoom(
        @Valid @RequestBody CreateRoomRequest request
    ) {
        return ResponseEntity.ok(roomService.addRoom(request));
    }

    @GetMapping("/{roomId}")
    @ResponseBody
    public Room getRoomById(@PathVariable Integer roomId) {
        Room room = roomService.getRoomById(roomId);
        return room;
    }

    @PostMapping("/{roomId}/book")
    public ResponseEntity<BookingResponse> bookRoom(
        @RequestHeader(value = "Authorization") String accessToken,
        @PathVariable Integer roomId,
        @Valid @RequestBody BookingRequest request
    ) {
        String email = jwtService.extractUserEmail(accessToken.substring(7));
        User user = authService.getUserByEmail(email);
        Booking booking = roomService.bookingRoom(roomId, request, user);

        BookingResponse bookingResponse = this.bookingService.convertBookingToBookingResponse(booking);
        return ResponseEntity.ok(bookingResponse);
    }

    @PostMapping("/search")
    public List<RoomInformationResponse> searchRooms(@Valid @RequestBody SearchAvilableRoomRequest request) {
        List<Room> rooms = roomService.searchRoomsByRequest(request);
        List<RoomInformationResponse> roomInformationResponses = convertRoomToRoomInformationResponse(rooms);
        return roomInformationResponses;
    }
}
