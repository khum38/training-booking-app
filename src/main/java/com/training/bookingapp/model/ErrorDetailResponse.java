package com.training.bookingapp.model;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
@AllArgsConstructor
public class ErrorDetailResponse {
    
    private LocalDateTime timestamp;
    private String message;
    private String details;
    private String errorCode;
}
