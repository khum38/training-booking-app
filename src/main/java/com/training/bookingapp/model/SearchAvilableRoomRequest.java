package com.training.bookingapp.model;

import java.util.Date;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchAvilableRoomRequest {
    private Integer guestNumber;
    private Date date;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
