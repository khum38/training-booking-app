package com.training.bookingapp.model;

import java.time.LocalDateTime;
import java.util.Date;

import lombok.AllArgsConstructor;
// import com.training.bookingapp.dbmodel.User;
// import com.training.bookingapp.dbmodel.Room;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingResponse {
    private Integer id;
    private RoomInformationResponse room;
    private UserResponse organizer;
    private Date date;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String description;
    private String title;
    private Integer guestNumber;
    
}
