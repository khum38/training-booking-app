package com.training.bookingapp.model;

import java.sql.Time;
// import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// import com.training.bookingapp.dbmodel.Booking;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomInformationResponse {
    private Integer id;
    private String name;
    private String description;
    private Integer capacity;
    private Time openTime;
    private Time closeTime;
    // private List<Booking> bookings;
}
