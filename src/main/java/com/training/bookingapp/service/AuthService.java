package com.training.bookingapp.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

// import org.springframework.security.crypto.password.PasswordEncoder;

import com.training.bookingapp.repository.UserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.training.bookingapp.model.AuthenticationRequest;
import com.training.bookingapp.model.AuthenticationResponse;
import com.training.bookingapp.config.JwtService;
import com.training.bookingapp.model.RegisterRequest;
import com.training.bookingapp.dbmodel.Role;
import com.training.bookingapp.dbmodel.User;
import com.training.bookingapp.exception.AuthServiceException;


@Service
@RequiredArgsConstructor
public class AuthService {
    // private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    
    public AuthenticationResponse register(RegisterRequest request){
        boolean userExists = userRepository.findByEmail(request.getEmail()).isPresent();
        if (userExists) {
            throw new AuthServiceException(
                "User already exists",
                "A001",
                "User " + request.getEmail() + " already exists"
            );
        }

        String encodedPassword = passwordEncoder.encode(request.getPassword());

        User newUser = User.builder()
            .email(request.getEmail())
            .password(encodedPassword)
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .role(Role.USER)
            .build();

        userRepository.save(newUser);
        var jwtToken = this.jwtService.generateToken(newUser);

        AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwtToken);
        return authenticationResponse;
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request){
        User user = this.getUserByEmail(request.getEmail());
        
        try {
            authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    request.getEmail(),
                    request.getPassword()
                )
            );
        } catch (Exception e) {
            throw new AuthServiceException(
                "Invalid credentials",
                "A002",
                "Invalid credentials"
            );
        }
        var jwtToken = this.jwtService.generateToken(user);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwtToken);
        return authenticationResponse;
    }

    public User getUserByEmail(String email){
        return userRepository.findByEmail(email)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
