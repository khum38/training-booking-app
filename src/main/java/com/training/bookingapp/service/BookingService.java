package com.training.bookingapp.service;

import java.util.List;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

import com.training.bookingapp.repository.BookingRepository;
import com.training.bookingapp.dbmodel.Booking;
import com.training.bookingapp.dbmodel.Room;
import com.training.bookingapp.dbmodel.User;
import com.training.bookingapp.exception.ResourceNotFoundException;
import com.training.bookingapp.model.BookingResponse;
import com.training.bookingapp.model.RoomInformationResponse;
import com.training.bookingapp.model.UserResponse;


@Service
@RequiredArgsConstructor
public class BookingService {
    
    private final BookingRepository bookingRepository;

    public Booking getBookingById(Integer bookingId) {
        final Booking booking = this.bookingRepository.findById(bookingId).orElseThrow(
            () -> new ResourceNotFoundException("Booking not found", "B001", "Booking id = " + bookingId + " not found")
        );
        return booking;
    }

    public List<Booking> getBookingByOrganizerId(Integer organizerId) {
        final List<Booking> bookings = this.bookingRepository.findAllByOrganizerId(organizerId).get();
        return bookings;
    }

    public void deleteBookingById(Integer bookingId) {
        this.bookingRepository.deleteById(bookingId);
    }

    public BookingResponse convertBookingToBookingResponse(Booking booking) {
        Room room = booking.getRoom();
        User organizer = booking.getOrganizer();

        return BookingResponse.builder()
            .id(booking.getId())
            .organizer(
                UserResponse.builder()
                    .id(organizer.getId())
                    .firstName(organizer.getFirstName())
                    .lastName(organizer.getLastName())
                    .email(organizer.getEmail())
                    .role(organizer.getRole())
                    .build()
            )
            .room(
                RoomInformationResponse.builder()
                    .id(room.getId())
                    .name(room.getName())
                    .capacity(room.getCapacity())
                    .description(room.getDescription())
                    .openTime(room.getOpenTime())
                    .closeTime(room.getCloseTime())
                    .build()
            )
            .guestNumber(booking.getGuestNumber())
            .date(booking.getDate())
            .startDateTime(booking.getStartDateTime())
            .endDateTime(booking.getEndDateTime())
            .title(booking.getTitle())
            .description(booking.getDescription())
            .build();
    }
}
