package com.training.bookingapp.service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Collections;

import org.springframework.stereotype.Service;

import com.training.bookingapp.dbmodel.Booking;
import com.training.bookingapp.dbmodel.Room;
import com.training.bookingapp.dbmodel.User;
import com.training.bookingapp.exception.ResourceNotFoundException;
import com.training.bookingapp.model.BookingRequest;
import com.training.bookingapp.model.CreateRoomRequest;
import com.training.bookingapp.model.SearchAvilableRoomRequest;
import com.training.bookingapp.model.RoomInformationResponse;
import com.training.bookingapp.repository.BookingRepository;
import com.training.bookingapp.repository.RoomRepository;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;
    private final BookingRepository bookingRepository;

    public List<Room> getAllRooms() {
        final List<Room> rooms = this.roomRepository.findAll();
        return rooms;
    }

    public Room getRoomById(Integer id) {
        final Room room = this.roomRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("Room not found", "R001", "Room id = " + id + " not found")
        );
        return room;
    }

    public List<Room> searchRoomsByRequest(SearchAvilableRoomRequest request) {
        final List<Room> rooms = this.searchRoom(request.getGuestNumber(), request.getDate(), request.getStartTime(), request.getEndTime());
        return rooms;
    }

    private List<Room> searchRoom(Integer guestNumber, Date date, LocalDateTime startTime, LocalDateTime endTime) {
        final List<Room> filteredRooms = this.roomRepository
            .findAllByCapacityAndAvilable(guestNumber, startTime, endTime)
            .orElse(Collections.<Room>emptyList());
        return filteredRooms;
    }

    public Booking bookingRoom(Integer roomId, BookingRequest request, User user) {
        final Room room = this.getRoomById(roomId);
        final List<Room> rooms = this.searchRoom(request.getGuestNumber(), request.getDate(), request.getStartTime(), request.getEndTime());
        
        if (!rooms.contains(room)) {
            throw new ResourceNotFoundException("Room not available", "R002", "Room id = " + roomId + " not available");
        }
        final Booking booking = Booking.builder()
            .room(room)
            .organizer(user)
            .guestNumber(request.getGuestNumber())
            .date(request.getDate())
            .startDateTime(request.getStartTime())
            .endDateTime(request.getEndTime())
            .title(request.getTitle())
            .description(request.getDescription())
            .createDateTime(LocalDateTime.now())
            .build();
        this.bookingRepository.save(booking);
        return booking;
    }

    public Room addRoom(CreateRoomRequest roomRequest) {
        Room room = Room.builder()
            .name(roomRequest.getName())
            .capacity(roomRequest.getCapacity())
            .description(roomRequest.getDescription())
            .openTime(roomRequest.getOpenTime())
            .closeTime(roomRequest.getCloseTime())
            .build();
        
        this.roomRepository.save(room);
        return room;
    }

    public RoomInformationResponse convertRoomToRoomInformationResponse(Room room) {
        return RoomInformationResponse.builder()
            .id(room.getId())
            .name(room.getName())
            .capacity(room.getCapacity())
            .description(room.getDescription())
            .openTime(room.getOpenTime())
            .closeTime(room.getCloseTime())
            .build();
    }
}
