package com.training.bookingapp.exception;

public class ResourceNotFoundException extends GenericException {

    public ResourceNotFoundException(String message, String errorCode, String details) {
        super(message, errorCode, details);
    }
    
}
