package com.training.bookingapp.exception;

public class GenericException extends RuntimeException {

    private String errorCode;
    private String details;

    public GenericException(String message, String errorCode, String details) {
        super(message);
        this.errorCode = errorCode;
        this.details = details;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getDetails() {
        return details;
    }
}
