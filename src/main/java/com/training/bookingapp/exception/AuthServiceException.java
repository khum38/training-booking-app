package com.training.bookingapp.exception;


public class AuthServiceException extends GenericException {

    public AuthServiceException(String message, String errorCode, String details) {
        super(message, errorCode, details);
    }
}
