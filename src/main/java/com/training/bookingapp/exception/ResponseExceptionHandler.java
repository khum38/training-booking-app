package com.training.bookingapp.exception;

import java.time.LocalDateTime;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.training.bookingapp.model.ErrorDetailResponse;


@ControllerAdvice // This annotation allows this class to be shared across multiple controllers
public class ResponseExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetailResponse> handleException(Exception e) {
        ErrorDetailResponse errorResponse = ErrorDetailResponse.builder()
            .timestamp(LocalDateTime.now())
            .message(e.getMessage())
            .details(e.getLocalizedMessage())
            .errorCode("00")
            .build()
        ;
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(AuthServiceException.class)
    public ResponseEntity<ErrorDetailResponse> handleAuthServiceException(AuthServiceException e) {
        ErrorDetailResponse errorResponse = ErrorDetailResponse.builder()
            .timestamp(LocalDateTime.now())
            .message(e.getMessage())
            .details(e.getDetails())
            .errorCode(e.getErrorCode())
            .build()
        ;
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorDetailResponse> handleResourceNotFoundException(ResourceNotFoundException e) {
        ErrorDetailResponse errorResponse = ErrorDetailResponse.builder()
            .timestamp(LocalDateTime.now())
            .message(e.getMessage())
            .details(e.getMessage())
            .errorCode(e.getErrorCode())
            .build()
        ;
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ErrorDetailResponse> handleUsernameNotFoundException(UsernameNotFoundException e) {
        ErrorDetailResponse errorResponse = ErrorDetailResponse.builder()
            .timestamp(LocalDateTime.now())
            .message(e.getMessage())
            .details(e.getMessage())
            .errorCode("A003")
            .build()
        ;
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }
    
}
