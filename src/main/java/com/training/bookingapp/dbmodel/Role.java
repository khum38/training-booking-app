package com.training.bookingapp.dbmodel;

public enum Role {
    USER,
    ADMIN
}
