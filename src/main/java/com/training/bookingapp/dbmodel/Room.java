package com.training.bookingapp.dbmodel;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.micrometer.common.lang.Nullable;

import java.text.SimpleDateFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Size(min = 3, max = 50)
    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private List<Booking> bookings;
    
    @Min(1)
    private Integer capacity;

    @Nullable
    private String description;

    @Nullable
    private Time openTime;

    @Nullable
    private Time closeTime;

    public List<Booking> getBookings() {
        return this.bookings;
    }

    public Room(Integer id, String name, Integer capacity, String description, String openTimeString, String closeTimeString) {
        super();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        try {
            Date openDateTime = sdf.parse(openTimeString);
            Date closeDateTime = sdf.parse(closeTimeString);
            this.openTime = new Time(openDateTime.getTime());
            this.closeTime = new Time(closeDateTime.getTime());
        } catch (Exception e) {
            this.openTime = null;
            this.closeTime = null;
        }

        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.description = description;
    }
}
