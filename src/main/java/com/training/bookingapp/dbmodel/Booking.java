package com.training.bookingapp.dbmodel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;

import java.time.LocalDateTime;
import java.util.Date;

import io.micrometer.common.lang.Nullable;



@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "organizer_user_id")
    private User organizer;

    private String title;

    @Nullable
    private String description;

    private LocalDateTime startDateTime;

    private LocalDateTime endDateTime;

    private Date date;

    private LocalDateTime createDateTime;

    @Min(1)
    private Integer guestNumber;
}
